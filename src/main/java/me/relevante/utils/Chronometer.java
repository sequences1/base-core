package me.relevante.utils;

import java.util.concurrent.TimeUnit;

public class Chronometer {

    TimeUnit timeUnit;
    long startTime;

    public Chronometer() {
        this(TimeUnit.MILLISECONDS);
    }

    public Chronometer(TimeUnit timeUnit) {
        this.timeUnit = timeUnit;
        this.startTime = System.nanoTime();
    }

    public String getElapsedTimeText() {
        long endTime = System.nanoTime();
        long elapsed = endTime - startTime;
        String message = timeUnit.convert(elapsed, TimeUnit.NANOSECONDS) + " " + timeUnit.toString().toLowerCase();
        return message;
    }
}
