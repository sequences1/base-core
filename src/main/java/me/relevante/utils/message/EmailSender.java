package me.relevante.utils.message;

/**
 * Created by daniel-ibanez on 30/09/16.
 */
public interface EmailSender {
    void sendMessage(String to, String subject, String body) throws Exception;
}
