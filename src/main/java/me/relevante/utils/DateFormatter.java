package me.relevante.utils;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

@Component
public class DateFormatter {

    private SimpleDateFormat simpleDateFormat;
    private DateTimeFormatter dateTimeFormatter;

    @Autowired
    public DateFormatter(@Value("${custom.dateFormat}") String dateFormat) {
        this.simpleDateFormat = new SimpleDateFormat(dateFormat);
        this.dateTimeFormatter = DateTimeFormatter.ofPattern(dateFormat);
    }

    public String formatDate(String unformattedDate) {
        if (StringUtils.isBlank(unformattedDate)) {
            return "";
        }
        Date date;
        if (unformattedDate.contains("-") || unformattedDate.contains("/")) {
            date = new Date(unformattedDate);
        }
        else {
            date = new Date(Long.parseLong(unformattedDate));
        }
        return formatDate(date);
    }

    public String formatDate(Date date) {
        if (date == null) {
            return "";
        }
        String formattedDate = simpleDateFormat.format(date);
        return formattedDate;
    }

    public LocalDateTime parseString(String dateText) {
        if (dateText == null) {
            return null;
        }
        LocalDateTime parsedDate = LocalDateTime.parse(dateText, dateTimeFormatter);
        return parsedDate;
    }
}
