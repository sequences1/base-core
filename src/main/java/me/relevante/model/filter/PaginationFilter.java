package me.relevante.model.filter;

import java.util.List;

public class PaginationFilter<T> extends AbstractFilter<T> implements Filter<T> {

    private int firstElement;
    private int maxElements;

    public PaginationFilter(int firstElement,
                            int maxElements) {
        this.firstElement = firstElement;
        this.maxElements = maxElements;
    }

    public PaginationFilter(int firstElement) {
        this(firstElement, Integer.MAX_VALUE);
    }

    @Override
    protected List<T> select(List<T> objects) {
        int fromIndex = firstElement;
        int toIndex = Math.min(firstElement + maxElements, objects.size());
        List<T> selectedUsers = objects.subList(fromIndex, toIndex);
        return selectedUsers;
    }

}
