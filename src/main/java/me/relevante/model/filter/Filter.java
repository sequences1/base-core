package me.relevante.model.filter;

import java.util.List;

public interface Filter<T> {

    void setNext(Filter<T> next);
    List<T> execute(List<T> users);

}
