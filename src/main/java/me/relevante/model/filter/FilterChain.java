package me.relevante.model.filter;

import java.util.List;

public class FilterChain<T> {

    private Filter<T> firstUserSelector;
    private Filter<T> lastUserSelector;

    public List<T> execute(List<T> users) {
        return firstUserSelector.execute(users);
    }

    public FilterChain add(Filter<T> userSelector) {
        if (this.firstUserSelector == null)
            firstUserSelector = userSelector;
        if (lastUserSelector != null)
            lastUserSelector.setNext(userSelector);
        lastUserSelector = userSelector;
        return this;
    }
}
