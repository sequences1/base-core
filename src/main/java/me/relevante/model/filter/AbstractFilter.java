package me.relevante.model.filter;

import java.util.List;

public abstract class AbstractFilter<T> implements Filter<T> {

    protected Filter<T> next;

    public void setNext(Filter<T> next){
        this.next = next;
    }

    public List<T> execute(List<T> objectsToFilter){

        List<T> filteredObjects = select(objectsToFilter);
        if (next != null) {
            filteredObjects = next.execute(filteredObjects);
        }

        return filteredObjects;
    }

    abstract protected List<T> select(List<T> users);

}
