package me.relevante.model.filter;

import me.relevante.model.validation.Validator;

import java.util.ArrayList;
import java.util.List;

public class ValidationFilter<T> extends AbstractFilter<T> implements Filter<T> {

    private Validator<T> validator;

    public ValidationFilter(Validator<T> validator) {
        this.validator = validator;
    }

    @Override
    protected List<T> select(List<T> users) {
        List<T> selectedUsers = new ArrayList<>();
        for (T user : users) {
            if (validator.isValid(user)) {
                selectedUsers.add(user);
            }
        }
        return selectedUsers;
    }
}
