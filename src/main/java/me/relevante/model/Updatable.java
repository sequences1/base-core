package me.relevante.model;

public interface Updatable<T> {

    void updateDataFrom(T sourceObject);
    void completeDataFrom(T sourceObject);
}
